<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Contact\BookingRequest;
use App\Mail\Frontend\Contact\Booking as BookingMail;
use App\Models\Booking\Booking;
use Illuminate\Support\Facades\Mail;

/**
 * Class ContactController.
 */
class BookingController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.booking');
    }

    /**
     * @param BookingRequest $request
     *
     * @return mixed
     */
    public function send(BookingRequest $request)
    {
        Mail::send(new BookingMail($request));
        Booking::create($request->all());

        return redirect()->back()->withFlashSuccess(__('alerts.frontend.contact.sent'));
    }
}
