<?php

namespace App\Models\Booking;

use App\Models\RecordingModel;

/**
 * Class Booking.
 */
class Booking extends RecordingModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'nick',
        'email',
        'dni',
        'message',
        'phone',
    ];
}
