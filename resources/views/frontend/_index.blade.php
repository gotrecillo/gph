@extends('frontend.layouts.web')

@section('header')
    <div class="clearfix">


        <section id="slider" class="slider-parallax full-screen with-header force-full-screen clearfix">

            <div class="slider-parallax-inner">

                <div class="full-screen force-full-screen"
                     style="background: url('/img/frontend/home/portada_reduced.jpg') center center no-repeat; background-size: cover;">

                    <a href="http://galapagar.es/" target="_blank" class="logo_ayto"> <img
                            src="/img/frontend/home/galpitofreaklogo-01.png"/></a>
                    <img src="/img/frontend/logo_asoc.png" class="logo_asoc"/>

                    <div class="container clearfix">
                        <div class="emphasis-title vertical-middle center">
                            <h1 data-animate="fadeInUp" class="portada">2ª EDICIÓN <strong>GALAPAGAR LAN PARTY</strong>
                            </h1>
                            <h3 data-animate="fadeInUp" class="portada">30 de abril, 1, 2 y 3 de mayo</h3>
                            <div class="center" data-animate="bounceIn">
                                <a href="{{route('frontend.booking')}}"
                                   class="button button-rounded button-white button-light button-large">
                                    ¡VEN A PASARLO BIEN!
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        @include('frontend.includes.web_nav')
        <div class="clear"></div>

    </div>
@endsection


@section('content')

    <section id="content" class="home-page-wrap">

        <div class="content-wrap nopadding">

            <div class="section parallax full-screen nomargin noborder"
                 style="background-image: url('/img/frontend/logo_reduced.png'); background-repeat: no-repeat; background-position-y: 80%; background-position-x: 90%; background-size: 500px;"
                 data-stellar-background-ratio="0.4">
                <div class="vertical-middle">
                    <div class="container clearfix">

                        <div class="col_three_fifth nobottommargin">

                            <div class="emphasis-title galaparty-description">
                                <h2>Galapagar LAN Party</h2>
                                <p class="lead topmargin-sm">
                                    Galapagar LAN Party es un evento muy esperado por profesionales y aficionados del
                                    maravilloso mundo friki y tecnológico, donde compartirán su pasión durante 4 días.
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-12 nopadding common-height">

                <div class="col-md-4 dark col-padding ohidden" style="background-color: #258949;">
                    <div>
                        <h3 class="uppercase" style="font-weight: 600;">Dónde</h3>
                        <p style="line-height: 1.8;">
                            Velódromo de Galapagar
                            <br/>
                            Calle Guadarrama s/n
                            <br/>
                            Galapapagar 28260, Madrid
                        </p>
                        <a href="{{route('frontend.ubicacion')}}"
                           class="button button-border button-light button-rounded uppercase nomargin">Cómo
                            llegar</a>
                        <i class="icon-location bgicon"></i>
                    </div>
                </div>

                <div class="col-md-4 dark col-padding ohidden" style="background-color: #36506b;">
                    <div>
                        <h3 class="uppercase" style="font-weight: 600;">Cuándo</h3>
                        <p style="line-height: 1.8;">
                            30 de abril, 1, 2 y 3 de mayo
                        </p>
                        <div class="masonry-thumbs col-2" data-big="3" data-lightbox="gallery">
                            <a href="/img/frontend/calendario.png" data-lightbox="gallery-item"
                               style="width: 570px; position: absolute; left: 0px; top: 0px;">
                                <img class="image_fade" src="/img/frontend/calendario.png" style="height: 150px;"/>
                            </a>
                        </div>
                        <i class="icon-calendar bgicon"></i>
                    </div>
                </div>

                <div class="col-md-4 dark col-padding ohidden" style="background-color: #b90714;">
                    <div>
                        <h3 class="uppercase" style="font-weight: 600;">Cuánto</h3>
                        <p style="line-height: 1.8;">
                            Entrada para cuatro días 25€.
                            <br/>
                            Entrada para dos días 20€.
                            <br/>
                            Oferta por grupo/Clan: 20€ (ver <a href="{{route('frontend.participar')}}"
                                                               style="color: rgb(54, 80, 107);">info</a>)
                        </p>
                        <a href="{{route('frontend.booking')}}"
                           class="button button-border button-light button-rounded uppercase nomargin">Inscríbete</a>
                        <i class="icon-money bgicon"></i>
                    </div>
                </div>

                <div class="clear"></div>

            </div>

            <div class="clear"></div>

            <div class="section parallax full-screen dark nomargin noborder"
                 style="background-image: url('/img/frontend/home/04.jpg');" data-stellar-background-ratio="0.4">
                <div class="vertical-middle">
                    <div class="container clearfix">

                        <div class="row">
                            <div class="col-md-3 bottommargin-sm center" data-animate="bounceIn">
                                <div class="counter counter-large counter-lined">
                                    <span data-from="0" data-to="30" data-refresh-interval="50"
                                          data-speed="1000"></span>
                                </div>
                            </div>

                            <div class="col-md-1 bottommargin-sm center" data-animate="bounceIn">
                                <div class="counter counter-large counter-lined"> -</div>
                            </div>

                            <div class="col-md-3 bottommargin-sm center" data-animate="bounceIn" data-delay="200">
                                <div class="counter counter-large counter-lined">
                                    <span>0</span>
                                    <span data-from="0" data-to="4" data-refresh-interval="100" data-speed="100"></span>
                                </div>
                            </div>

                            <div class="col-md-1 bottommargin-sm center" data-animate="bounceIn">
                                <div class="counter counter-large counter-lined"> -</div>
                            </div>

                            <div class="col-md-3 bottommargin-sm center" data-animate="bounceIn" data-delay="400">
                                <div class="counter counter-large counter-lined">
                                    <span data-from="2000" data-to="2020" data-refresh-interval="25"
                                          data-speed="1500"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 topmargin-lg center" data-animate="bounceIn">
                                <a href="{{route('frontend.booking')}}"
                                   class="button button-rounded button-white button-light button-large">
                                    INSCRÍBETE YA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section parallax full-screen dark nomargin noborder"
                 style="background-color: #585858;" data-stellar-background-ratio="0.4">
                <div class="vertical-middle">
                    <div class="container clearfix">

                        <div class="col_three_fifth nobottommargin">

                            <iframe width="500" height="281" src="https://www.youtube.com/embed/om_Tx_w0nNY"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>

                        </div>

                        <div class="col_two_fifth col_last nobottommargin">
                            <div class="emphasis-title">
                                <h2>Galapagar LAN Party 2019</h2>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
