@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched contacto" style="background-image: url('/img/frontend/contact.jpg')">
@endsection

@section('header')
    <div id="wrapper" class="clearfix">
        @include('frontend.includes.web_nav')
        <div class="clear"></div>
    </div>
@endsection
@section('content')
    <div class="contact-form container">
        <div class="row">
            <div class="align-self-center col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2>Contacta con nosotros</h2>
                    </div>
                    <div class="card-body">
                        {{ html()->form('POST', route('frontend.contact.send'))->open() }}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label('Nombre')->for('name') }}

                                    {{ html()->text('name')
                                        ->class('form-control')
                                        ->placeholder('Nombre')
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label('Email')->for('email') }}

                                    {{ html()->text('email')
                                        ->class('form-control')
                                        ->placeholder('Email')
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label('Mensaje')->for('message') }}

                                    {{ html()->textarea('message')
                                        ->class('form-control')
                                        ->placeholder('Mensaje')
                                        ->attribute('rows', 4) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group checkbox-field">
                                    <span class="privacy"> He leído y acepto la </span><a
                                        href="{{route('frontend.politica-de-privacidad')}}" target="_blank">
                                        Política de Privacidad</a>
                                    <input type="checkbox" name="privacy"/>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ form_submit('Enviar') }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                        {{ html()->form()->close() }}
                    </div><!--card-body-->
                </div><!--card-->
            </div><!--col-->
        </div>
    </div><!--row-->
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
