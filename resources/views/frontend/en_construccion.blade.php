@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched footer-top">
    @endsection

    @section('header')
        <div id="wrapper" class="clearfix">
            @include('frontend.includes.web_nav')
            <div class="clear"></div>
        </div>
    @endsection

    @section('content')
        <section id="content" class="en-construccion-wrapp">
            <div class="content-wrap nopadding">
                <div class="container clearfix">
                    <div class="construction-photo"
                         style="background-position-x: center; background-image: url('/img/frontend/17435_reduced.jpg'); background-size: contain; background-repeat: no-repeat;
    height: 800px;">
                    </div>
                    <div class="col_three_fifth nobottommargin">
                        <div class="emphasis-title">
                            <div class="construccion"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
