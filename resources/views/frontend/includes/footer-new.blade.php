<footer id="footer" class="dark">

    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                <img src="/img/frontend/colaboracion.png" style="height: 107px;">
                <br/><br/>
                <div class="copyright-links"><a target="_blank" href="{{route('frontend.aviso-legal')}}">Aviso legal</a>
                    / <a
                        href="{{route('frontend.politica-de-privacidad')}}" target="_blank">Política de privacidad</a>
                </div>
            </div>
            <div class="col_half socials" style="position: absolute;right: 0; margin-top: 5%;">
                <a href="https://twitter.com/LanPartyGala?s=20" target="_blank" class="twitter">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/lanpartygala/" target="_blank" class="insta">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="https://www.twitch.tv/lanpartygalapagar" target="_blank" class="twitch">
                    <i class="fab fa-twitch"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCk8tP69LEz-LNF1NOe7Wabw" target="_blank" class="youtube">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
            <div class="col_half col_last right" style="margin-top: 5%;">
                <div class="fright red-text clearfix">
                    <a href="mailto:info@galapagarlanparty.org">
                        <i class="icon-envelope2"></i> info@galapagarlanparty.org <span class="middot">&middot;</span>
                    </a>
                    <a target="_blank"
                       href="https://api.whatsapp.com/send?phone=34633945691">
                        <i class="fab fa-whatsapp"></i> 633 94 56 91 <span class="middot">&middot;</span>
                    </a>
                    <a href="#">
                        <i class="fab fa-telegram"></i> 633 94 56 91 <span class="middot">&middot;</span>
                    </a>
                </div>
                <div class="clear"></div>
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer>
