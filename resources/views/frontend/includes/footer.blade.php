<div class="container clearfix">
    <div class="footer-flex">
        <div class="footer-flex-item">
            <img src="/img/frontend/colaboracion.png" style="height: 107px;">
            <br/><br/>
            <div class="copyright-links"><a target="_blank" href="{{route('frontend.aviso-legal')}}">Aviso legal</a>
                / <a
                    href="{{route('frontend.politica-de-privacidad')}}" target="_blank">Política de privacidad</a>
            </div>
        </div>
        <div class="footer-flex-item">
            <div class="socials">
                <a href="https://twitter.com/LanPartyGala?s=20" target="_blank" class="twitter">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="https://www.instagram.com/lanpartygala/" target="_blank" class="insta">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="https://www.twitch.tv/lanpartygalapagar" target="_blank" class="twitch">
                    <i class="fab fa-twitch"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCk8tP69LEz-LNF1NOe7Wabw" target="_blank" class="youtube">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
            <div class="copyright-links">
                <div>
                    <a href="mailto:info@galapagarlanparty.org">
                        <i class="icon-envelope2"></i> info@galapagarlanparty.org
                    </a>
                </div>
                <div>
                    <a target="_blank"
                       href="https://api.whatsapp.com/send?phone=34633945691">
                        <i class="fab fa-whatsapp"></i> 633 94 56 91
                    </a>
                </div>
                <div>
                    <a href="http://t.me/lanpartygala" target="_blank">
                        <i class="fab fa-telegram"></i> Canal de telegram
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


