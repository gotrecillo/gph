<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <!-- Stylesheets
    ============================================= -->
    <link
        href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
        rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/dark.css" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/font-icons.css" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/magnific-popup.css" type="text/css"/>
    <link rel="stylesheet" href="/canvas/css/custom.css" type="text/css"/>

    <link rel="stylesheet" href="/canvas/css/responsive.css" type="text/css"/>
    <meta property="og:image" content="https://www.galapagarlanparty.org/img/frontend/17435.jpg" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <script src="https://kit.fontawesome.com/92b59a278e.js" crossorigin="anonymous"></script>
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
