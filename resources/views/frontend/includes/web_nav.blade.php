<header id="header" class="transparent-header">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="/" class="standard-logo" data-dark-logo="/img/frontend/logo_reduced.png"><img
                        src="/img/frontend/logo_reduced.png" alt="logo"></a>
                <a href="/" class="retina-logo" data-dark-logo="/img/frontend/logo_reduced.png"><img
                        src="/img/frontend/logo_reduced.png" alt="logo"></a>
            </div><!-- #logo end -->

            <nav id="primary-menu">
                <ul>
                    <li class="current">
                        <a href="#">
                            <div>Galapartízate</div>
                        </a>
                        <ul>
                            <li><a href="{{route('frontend.info')}}">
                                    <div>Información básica</div>
                                </a></li>
                            <li><a href="{{route('frontend.que-traer')}}">
                                    <div>Qué traer</div>
                                </a></li>
                            <li><a href="{{route('frontend.participar')}}">
                                    <div>Cómo participar</div>
                                </a></li>
                            <li><a href="{{route('frontend.ubicacion')}}">
                                    <div>Lugar del evento</div>
                                </a></li>
                            <li><a href="{{route('frontend.normas')}}">
                                    <div>Normas</div>
                                </a></li>
                            <li><a href="/autorizacion_menores.pdf" target="_blank">
                                    <div>Autorización menores</div>
                                </a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <div>Actividades</div>
                        </a>
                        <ul>
                            <li><a href="{{route('frontend.juegos-y-torneos')}}">
                                    <div>Juegos y Torneos</div>
                                </a></li>
                            <li><a href="{{route('frontend.conferencias-y-talleres')}}">
                                    <div>Conferencias y Talleres</div>
                                </a></li>
                            <li><a href="{{route('frontend.concursos')}}">
                                    <div>Concursos</div>
                                </a></li>
                        </ul>
                    </li>
                    <li class="mega-menu"><a href="{{route('frontend.foro')}}">
                            <div>Foro</div>
                        </a></li>
                    <li class="mega-menu"><a href="{{route('frontend.contact')}}">
                            <div>Contacto</div>
                        </a></li>
                    <li><a href="{{route('frontend.acceso')}}">
                            <div><i class="icon-user"></i> Acceder</div>
                        </a></li>
                </ul>
            </nav><!-- primary-menu end -->

        </div>
    </div>
</header>
