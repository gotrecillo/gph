@extends('frontend.layouts.web')

@section('content')

    <section id="content" class="home-page-wrap">

        <div class="container">
            <div style="margin-top: 6%; border: 7px solid #bb1f1f; padding: 20px;">
                <h3>Se <strong>APLAZA</strong> la Galapagar Lan Party hasta 2022.</h3>

                <p>Mientras tanto haremos torneos online:
                    <br/>
                    <a href="https://asociacionlanparty.org/" target="_blank">
                        Apúntate de forma <strong>gratuita</strong> y gana grandes premios!
                    </a>
                </p>
            </div>
            <img src="/img/frontend/angry_fox_resized.png"/>

            <br/><br/>
        </div>
    </section>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
