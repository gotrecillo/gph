@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched info">
    @endsection

    @section('header')
        <div id="wrapper" class="clearfix">
            @include('frontend.includes.web_nav')
            <div class="clear"></div>
        </div>
    @endsection
    @section('content')
            <div class="container clearfix">
                <div class="col_three_fifth">
                    <div>
                        <h2>Información Galaparty: Zonas , salas, juegos, comida, bebida….</h2>
                        <p class="lead topmargin-sm">
                            La galapagar LAN party es una Fiesta LAN , se celebrará entre los días 30 de Abril, 1, 2, y 3 de Mayo en las instalaciones del polideportivo de galapagar, es un
                            evento que reúne a un grupo de personas con sus ordenadores para jugar,
                            compartir e intercambiar información, además de poder conocer gente, hacer
                            amigos y aprender de otras personas sobre tecnología
                        </p>

                        <h4>¿ Que se podrá hacer en la Galapagar lan party?</h4>
                        <p>
                            Principalmente , jugar a juegos en red local, pero hay muchas otras cosas,
                            compartir ficheros, recursos, trabajos, compartir información, cursos,
                            conferencias, etc. Además, estas reuniones sirven para que los usuarios se
                            conozcan cara a cara, puesto que la mayoría de las veces tan sólo se conocen a
                            través de Internet.
                            <br/><br/>
                            Es un lugar donde durante 4 días podremos disfrutar de una conexión de red local
                            de alta velocidad que servirá para poder jugar, compartir, navegar y participar
                            en las distintas actividades que realizaremos tanto a través de la red local
                            como en las distintas zonas lúdicas y formativas
                        </p>
                        <h4>Tendremos 4 zonas marcadas principalmente: </h4>
                        <ul>
                            <li>Zona de PC Lan para traer tu ordenador o consola junto a tu pantalla y
                                mandos si así lo deseas (zona de inscripción previa y con torneos
                                exclusivos)
                            </li>

                            <li> Zona de PS4: Dispondremos de varias máquinas de Playstation 4 (posiblemente
                                aumentaremos máquinas)con distintos juegos.
                            </li>
                            <ul>
                                <li>Se podrá participar tanto siendo inscrito en la party</li>
                                <li>Público podrá participar en los torneos sin estar inscritos en la Party,
                                    con reserva previa en las partidas
                                </li>
                            </ul>
                            <li>Zona de Juegos Móviles y Tablets: Dispondremos de un router wifi dedicado
                                para las partidas de Móviles y Tablets
                            </li>
                            <ul>
                                <li>Se podrá participar tanto siendo inscrito en la party</li>
                                <li>Público podrá participar en los torneos sin estar inscritos en la Party,
                                    con reserva previa en las partidas
                                </li>
                            </ul>
                            <li>Zona de juegos de mesa: Dispondremos de unas mesas y sillas con distintos
                                juegos de cartas que propondremos de cara a participar en partidas durante
                                los 4 días de la Party
                            </li>
                        </ul>
                        <br/>
                        <p> Dispondremos de 3 zonas de red, juegos, ocio + 1 sala de
                            conferencias/cursos/charlas + 1 sala para dormir + 1 comedor, también
                            dispondremos de servicio de bebidas, un Foodtruck y tienda informática para
                            “urgencias” o “necesidades” de última hora además de los distintos
                            patrocinadores en sus zonas de promoción:
                        </p>
                    </div>
                </div>
            </div>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
