@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched info">
    @endsection

    @section('header')
        <div id="wrapper" class="clearfix">
            @include('frontend.includes.web_nav')
            <div class="clear"></div>
        </div>
    @endsection
    @section('content')
        <div class="container clearfix">
            <div>
                <div>
                    <h2>Juegos & Torneos</h2>

                    <h4>Juegos PC</h4>
                    <p class="lead topmargin-sm">
                        Dentro de las distintas actividades que se realizarán en la Galapagar Lan Party, tendremos por
                        supuesto juegos de Pc, los distintos torneos serán explicados (formatos de competición a lo
                        largo de diferentes enlaces), la participación para tod@s aquell@s inscritos en la Galapagar Lan
                        Party será totalmente gratuita:
                    </p>
                    <ul>
                        <li>Fortnite</li>
                        <li>Counter Strike Global Offensive</li>
                        <li>League Of legends</li>
                        <li>Apex Legends</li>
                        <li>Hearthstone</li>
                        <li>Overwatch</li>
                        <li>Teamfight Tactics</li>
                        <li>Rocket league</li>
                    </ul>

                    <h4>Móviles/tablets</h4>
                    <p class="lead topmargin-sm">
                        Para participar debéis abonar 1€ por torneo, cada reenganche será de 1€.
                        <br/>
                        Cuando se acerque la fecha estará disponible la lista definitiva, horarios, y se abrirán las
                        preinscripciones
                    </p>
                    <ul>
                        <li>HAWK FREEDOM SQUADRON</li>
                        <li>CLASH ROYALE</li>
                        <li>BRAWL STARS</li>
                    </ul>

                    <h4>Juegos de mesa</h4>
                    <p class="lead topmargin-sm">
                        Dispondremos de unas mesas y sillas con distintos juegos de cartas que propondremos de cara a
                        participar en partidas durante los 4 días de la Party.
                    </p>
                    <ul>
                        <li>Zombicide</li>
                        <li>Massive Darkness</li>
                        <li>Jungle Speed</li>
                        <li>Bang</li>
                        <li>Código Secreto</li>
                        <li>Hombres lobo de Castronegro</li>
                        <li>Dominion</li>
                        <li>Virus</li>
                        <li>...y más!</li>
                    </ul>
                </div>
            </div>
        </div>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
