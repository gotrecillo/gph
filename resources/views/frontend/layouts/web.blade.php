<!DOCTYPE html>
<html dir="ltr" lang="es">
@include('frontend.includes.head')
@yield('body_opener')
<div class="page-container">
    <div class="header-container">
        @yield('header')
        @include('includes.partials.messages')
    </div>
    <div class="body-container">
        @yield('content')
    </div>
    <div class="footer-container">
    @yield('footer')
    <!-- Cookies -->
        <div id="cookie-bar" style="display: none;">
            <span class="message">
                Este sitio utiliza cookies.
                Para continuar en este sitio debes aceptar nuestra
                <a href="{{route('frontend.cookies')}}" target="_blank">política de cookies</a>
            </span>
            <span class="mobile">
                Este sitio utiliza cookies, <a href="{{route('frontend.cookies')}}" target="_blank">lee más</a>
            </span>
            <a href="javascript:void(0);" onclick="CloseCookie();">
                <label for="checkbox-cb" class="close-cb">x</label>
            </a>
        </div>
    </div>
</div>

<div id="gotoTop" class="icon-angle-up"></div>

<script type="text/javascript" src="/canvas/js/jquery.js"></script>
<script type="text/javascript" src="/canvas/js/plugins.js"></script>

<script type="text/javascript" src="/canvas/js/functions.js"></script>

<script>
    function getCookie(c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
            c_value = null;
        } else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    }

    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }


    function CloseCookie() {
        setCookie('cdccookie', '1', 365);
        document.getElementById("cookie-bar").style.display = "none";
    }


    $(document).ready(function () {
        if (getCookie('cdccookie') != "1") {
            document.getElementById("cookie-bar").style.display = "inline-block";
        }
    })
</script>
</body>
</html>
