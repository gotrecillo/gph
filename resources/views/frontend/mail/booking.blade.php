<p>@lang('strings.emails.booking.email_body_title')</p>

<p><strong>NOMBRE:</strong> {{ $request->name }}</p>
<p><strong>NICK:</strong> {{ $request->nick }}</p>
<p><strong>CLAN:</strong> {{ $request->clan }}</p>
<p><strong>DNI:</strong> {{ $request->dni }}</p>
<p><strong>EMAIL:</strong> {{ $request->email }}</p>
<p><strong>TELÉFONO:</strong> {{ $request->phone ?? 'N/A' }}</p>
<p><strong>MENSAJE:</strong> {{ $request->message }}</p>
<p><strong>Sitio para dormir
        :</strong> {{ $request->sleep ? 'Requiere sitio para dormir' : 'No necesita sitio para dormir' }}</p>
