@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched">
    @endsection

@section('header')
    <div id="wrapper" class="clearfix">
        @include('frontend.includes.web_nav')
        <div class="clear"></div>
    </div>
@endsection
@section('content')
    <section id="content">
        <div class="content-wrap nopadding">
            <div class="section parallax full-screen nomargin noborder"
                 style="background-image: url('/img/frontend/home/05.jpg');" data-stellar-background-ratio="0.4">
                <div class="vertical-middle">
                    <div class="container clearfix">
                        <div class="col_three_fifth nobottommargin">
                            <div class="emphasis-title">
                                <h2>Normas Generales de participación</h2>
                                <p class="lead topmargin-sm">
                                    A continuación os indicamos las normas y debéis ser conscientes de ellas durante el
                                    transcurso de la party. acceder a las instalaciones implica vuestro conocimiento y
                                    aceptación de las mismas.
                                </p>
                                <ul>
                                    <li><a href="#organizacion">LA ORGANIZACIÓN</a></li>
                                    <li><a href="#recinto">EL RECINTO
                                        </a></li>

                                    <li><a href="#horarios">HORARIOS y ACCESOS</a></li>

                                    <li><a href="#identificacion">IDENTIFICACIÓN</a></li>

                                    <li><a href="#comportamiento">COMPORTAMIENTO GENERAL – HIGIENE Y ORDEN</a></li>

                                    <li><a href="#material">CUIDADO DEL MATERIAL Y DE LAS INSTALACIONES</a></li>

                                    <li><a href="#electricidad">ELECTRICIDAD</a></li>

                                    <li><a href="#robos">ROBOS Y PÉRDIDAS</a></li>

                                    <li><a href="#fumar">FUMAR, BEBER ALCOHOL Y CONSUMIR DROGAS</a></li>

                                    <li><a href="#exterior">ZONAS EXTERIORES</a></li>

                                    <li><a href="#electrodomesticos">ELECTRODOMÉSTICOS Y OTROS APARATOS</a></li>

                                    <li><a href="#ruidos">RUIDOS, GRITOS Y ALTAVOCES</a></li>

                                    <li><a href="#focos">LÁSERES, FOCOS, LINTERNAS Y PROYECTORES – PATINES, PATINETES Y MONOPATINES –
                                        DRONES
                                        </a></li>
                                    <li><a href="#pernoctar">ESPACIO PARA PERNOCTAR – SILLAS Y COLCHONETAS
                                        </a></li>
                                    <li><a href="#puestos">PUESTOS Y ZONAS DE PASO – SOFTWARE Y PIRATERÍA
                                        </a></li>
                                    <li><a href="#equipamiento">EQUIPAMIENTO DE RED E INSTALACIONES DE COMUNICACIONES – USO DE LA RED
                                        </a></li>
                                    <li><a href="#control">ÁREAS DE CONTROL – ÁREAS Y ACTIVIDADES</a></li>

                                    <li><a href="#competiciones">COMPETICIONES Y PREMIOS</a></li>

                                    <li><a href="#sanciones">SANCIONES</a></li>
                                </ul>

                                <h4 id="organizacion">La organización</h4>
                                <p>
                                    La Galaparty es un evento que, tiene la colaboración de voluntarios que componen la
                                    Organización. Dichos voluntarios, a la par organizadores, se preocupan que el
                                    funcionamiento de todos los aspectos de la galaparty sea ótimo. Y, supervisan las
                                    actividades, los servicios prestados, y cualquier aspecto que pueda afectar a l@s
                                    participantes. Entre sus labores está, no sólo las orientadas a garantizar los
                                    derechos de los participantes, sino también las que tienen como objeto el hacer que
                                    se cumplan las normas generales de participación . Todos y cada uno de los
                                    organizadores cuentan con autoridad y respaldo de la Organización para llevar a cabo
                                    las acciones que consideren oportunas para garantizar el cumplimiento de las normas
                                    o la aplicación de las sanciones correspondientes. No obstante, existe la figura de
                                    responsable de seguridad que estará debidamente identificado y que velará por el
                                    cumplimiento de estas normas y asistirá a l@s participantes ante cualquier
                                    eventualidad que pudiera producirse.
                                    <br/><br/>
                                    Se prohíbe el uso de identificativos de la Organización a aquellas personas que no
                                    pertenezcan a la misma. Todo aquel que se haga pasar por miembro de la Organización
                                    (portando la camiseta característica o de cualquier otra manera) podrá ser
                                    sancionado.
                                </p>
                                <h4 id="recinto">El recinto</h4>
                                <p>
                                    El lugar de celebración del evento está cedido por parte del Ayuntamiento de
                                    Galapagar debido a la idoneidad de sus instalaciones, su ubicación, su tamaño, y sus
                                    facilidades de acceso. Si bien la Organización intentará en todo momento atender las
                                    demandas de los usuarios, existen ciertos servicios que quedan fuera de su alcance,
                                    y bajo la responsabilidad del propio ayuntamiento. El trato de la Organización con
                                    el ayuntamiento es fluido y constante, y estas demandas son, normalmente trasladadas
                                    y atendidas con celeridad.
                                    <br/><br/>
                                    Por otro lado, todo el material necesario para montar la party son alquilados o
                                    comprados. Esperamos que cada uno sepa la importancia que tiene el dejar todo el
                                    material del mismo modo en que lo encontró.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
