@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched participar">
    @endsection

    @section('header')
        <div id="wrapper" class="clearfix">
            @include('frontend.includes.web_nav')
            <div class="clear"></div>
        </div>
    @endsection
    @section('content')
        <div class="container clearfix">
            <div class="col_three_fifth">
                <div>
                    <br/>
                    <h2>Cómo participar en la Galaparty</h2>
                    <br/>
                    <p>
                        Rellena el <a href="{{route('frontend.booking')}}">
                            formulario de inscripción</a> para reservar tu entrada y nos pondremos en contacto.

                        <br/>
                        <br/>
                        El precio de la entrada para los <strong>4 días</strong> es de 25€.
                        <br/>
                        <br/>
                        El precio de la entrada par <strong>2 días</strong> es de 20€
                        <br/>
                        El pago se realizará mediante transferencia bancaria.

                    </p>
                    <p>
                        <strong>Oferta por grupo/Clan</strong>: 20€ por participante en grupos/clanes de 4 miembros
                        mínimo, la
                        inscripción debe ser con el mismo nombre de CLAN en el formulario de inscripción.
                    </p>
                </div>
            </div>
        </div>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
