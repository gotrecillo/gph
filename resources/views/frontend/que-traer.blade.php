@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched traer">
    @endsection

    @section('header')
        <div id="wrapper" class="clearfix">
            @include('frontend.includes.web_nav')
            <div class="clear"></div>
        </div>
    @endsection
    @section('content')
        <div class="container clearfix">
            <div class="col_three_fifth">
                <div>
                    <h2>Qué traer a la Galaparty</h2>
                    <p>
                        <a href="/Material_para_una_lan_party.pdf" target="_blank"> En este PDF</a> tenéis todo lo necesario para venir bien equipados a la Galapagar LAN Party.
                    </p>
                </div>
            </div>
        </div>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
