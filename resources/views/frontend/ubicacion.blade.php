@extends('frontend.layouts.web')

@section('body_opener')
    <body class="stretched ubicacion">
    @endsection

    @section('header')
        <div id="wrapper" class="clearfix">
            @include('frontend.includes.web_nav')
            <div class="clear"></div>
        </div>
    @endsection
    @section('content')
        <div class="container clearfix">
            <div>
                <div>
                    <h2>Localización Galaparty</h2>
                    Se celebrará en el Velódromo de Galapagar.
                    <br/>
                    A continuación os facilitamos información útil como gasolinera, zona de aparcamiento, localizaciones
                    de gps, paradas de bus y horarios, etc.
                    <br/><br/>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h3>Cómo llegar</h3>
                        <span>Calle Guadarrama s/n (o num 127 dependiendo del GPS)</span>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6060.3482288515825!2d-4.01065!3d40.581912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x943189e0e032a6d!2sVel%C3%B3dromo%20Galapagar!5e0!3m2!1ses!2ses!4v1578833756386!5m2!1ses!2ses"
                            width="400" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                    <div class="col-md-6">
                        <h3>Cómo llegar en autobús</h3>
                        <ul>
                            <li>En Autobús desde Collado Villalba (<a href="http://www.juliandecastro.es/"
                                                                      target="_blank">Julian de castro</a> autobus 630)
                                <a href="/img/frontend/bus-630" target="_blank"> ver horario </a>
                            </li>
                            <li>En Autobús desde Madrid:
                                Julian de Castro números: <a href="/img/frontend/bus-631" target="_blank">631</a> – <a
                                    href="/img/frontend/bus-632" target="_blank">632</a> y <a
                                    href="/img/frontend/bus-635" target="_blank">635</a>
                            </li>
                            <li>
                                <a href="https://galapagarlanparty.org/wp-content/uploads/2019/01/galaparty-sportgalapagar-galapagar_plano.pdf"
                                   target="_blank"> Plano de transportes y Mapa de Galapagar: Descarga
                                    AquíPDF</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3>Gasolinera</h3>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6060.160228747374!2d-4.01012!3d40.583987!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf37616b417a38a17!2sBP!5e0!3m2!1ses!2ses!4v1578835499363!5m2!1ses!2ses"
                            width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                    <div class="col-md-6" data-big="3" data-lightbox="gallery">

                        <h3>Aparcamientos disponibles</h3>
                        <li><a href="/img/frontend/aparcamiento-1.jpg" target="_blank" data-lightbox="gallery-item" >Aparcamiento entrada Polideportivo municipal (acceso Lan Party)</a></li>
                        <li><a href="/img/frontend/aparcamiento-2.jpg" target="_blank" data-lightbox="gallery-item" >Aparcamiento Piscina (a 100 metros entrada Polideportivo
                                municipal)</a></li>

                        <li><a href="/img/frontend/aparcamiento-3.jpg" target="_blank" data-lightbox="gallery-item" >Parking 1 Centro Cultural La pocilla (a 120 metros de la entrada a la
                                Party)</a></li>

                        <li><a href="/img/frontend/aparcamiento-4.jpg" target="_blank" data-lightbox="gallery-item" >Parking 2 Centro Cultural de la Pocilla (a 150 metros de la entrada a
                                la party)</a></li>
                        <li><a href="/img/frontend/aparcamiento-5.jpg" target="_blank" data-lightbox="gallery-item" >Parking Velódromo municipal de Galapagar (a 200m de la entrada a la
                                Party)</a></li>

                        <li><a href="/img/frontend/aparcamiento-6.jpg" target="_blank" data-lightbox="gallery-item" >Parking Enfrente del Velódromo municipal de Galapagar (a 200 m de la
                                entrada a la Party)</a></li>
                    </div>
                </div>
            </div>

        </div>
@endsection

@section('footer')
    @include('frontend.includes.footer')
@endsection
