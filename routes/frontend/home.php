<?php

use App\Http\Controllers\Frontend\BookingController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

Route::get('booking', [BookingController::class, 'index'])->name('booking');
Route::post('booking/send', [BookingController::class, 'send'])->name('booking.send');

Route::view('/cookies', 'frontend.cookies')->name('cookies');
Route::view('/aviso-legal', 'frontend.legal')->name('aviso-legal');
Route::view('/politica-de-privacidad', 'frontend.privacy')->name('politica-de-privacidad');

Route::view('/en-construccion', 'frontend.en_construccion')->name('en-construccion');
Route::view('/acceso', 'frontend.en_construccion')->name('acceso');
Route::view('/informacion-basica', 'frontend.info')->name('info');
Route::view('/que-traer', 'frontend.que-traer')->name('que-traer');

Route::view('/normas', 'frontend.en_construccion')->name('normas');
Route::view('/participar', 'frontend.participar')->name('participar');
Route::view('/ubicacion', 'frontend.ubicacion')->name('ubicacion');
Route::view('/juegos-y-torneos', 'frontend.juegos')->name('juegos-y-torneos');
Route::view('/conferencias-y-talleres', 'frontend.en_construccion')->name('conferencias-y-talleres');
Route::view('/concursos', 'frontend.en_construccion')->name('concursos');
Route::view('/foro', 'frontend.en_construccion')->name('foro');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
